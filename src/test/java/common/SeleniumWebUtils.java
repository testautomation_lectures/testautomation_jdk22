package common;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class SeleniumWebUtils {

	private static final List<Class<? extends Exception>> IGNORED_EXCEPTIONS = new ArrayList<>();
	static {
		IGNORED_EXCEPTIONS.add(StaleElementReferenceException.class);
	}
	
	private static final int POLLING_TIME = 1000;

	/**
	 * Wait for condition usign the fluent driver
	 * 
	 * @param waitCondition
	 */
	private static <T> T waitFor(WebDriver webDriver, ExpectedCondition<T> waitCondition, long timeout) {
		return new WebDriverWait(webDriver, Duration.ofMillis(timeout), Duration.ofMillis(POLLING_TIME)).until(waitCondition);
	}

	/**
	 * Waits for the specified condition until default timeout, ignoring exception
	 */
	private static <T> T waitFor(WebDriver webDriver, ExpectedCondition<T> waitCondition,
			List<Class<? extends Exception>> exceptionsToIgnore, long timeout) {
		return new WebDriverWait(webDriver, Duration.ofMillis(timeout), Duration.ofMillis(POLLING_TIME)).ignoreAll(exceptionsToIgnore).until(waitCondition);
	}
	
	public static boolean waitForLoading(WebDriver driver, By locator, long timeout) {
		try {
			try {
				waitFor(driver, ExpectedConditions.invisibilityOfElementLocated(locator), timeout);
			} catch (StaleElementReferenceException sexcp) {

			}
			return true;
		} catch (TimeoutException e) {
			return false; // element not found
		}
	}
    
	public static void waitForLoad(WebDriver _driver, long timeout) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver _driver) {
				return ((JavascriptExecutor) _driver).executeScript("return document.readyState").equals("complete");
			}
		};
		//_driver = _driver.switchTo().defaultContent();
		 (new WebDriverWait(_driver, Duration.ofMillis(timeout), Duration.ofMillis(POLLING_TIME))).until(pageLoadCondition);
	}
	
    public static boolean isDisplayed(WebDriver webDriver, By by, long timeout) {
        try {
            return findElement(webDriver, by, timeout).isDisplayed();
        } catch (TimeoutException e) {
            return false; // element not displayed
        }
    }

    public static boolean isDisplayed(WebDriver webDriver, WebElement element, long timeout) {
        try {
            return findElement(webDriver, element, timeout).isDisplayed();
        } catch (TimeoutException e) {
            return false; // element not displayed
        }
    }

    public static boolean isSelected(WebDriver webDriver, WebElement element, long timeout) {
        try {
            return findElement(webDriver, element, timeout).isSelected();
        } catch (TimeoutException e) {
            return false; // element not displayed
        }
    }

    public static boolean isEnabled(WebDriver webDriver, WebElement element, long timeout) {
        try {
            return findElement(webDriver, element, timeout).isEnabled();
        } catch (TimeoutException e) {
            return false; // element not displayed
        }
    }

    public static boolean isPresent(WebDriver webDriver, By by, long timeout) {
        try {
            return waitFor(webDriver, ExpectedConditions.presenceOfElementLocated(by), IGNORED_EXCEPTIONS, timeout).isDisplayed();
        } catch (TimeoutException e) {
            return false; // element not found
        }
    }

    public static boolean isNotPresent(WebDriver webDriver, By by, long timeout) {
        try {
            return waitFor(webDriver, ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(by)), IGNORED_EXCEPTIONS, timeout);
        } catch (TimeoutException e) {
            return false; // element found
        }
    }

    public static boolean isPresent(WebDriver webDriver, WebElement element, By sub_locator, long timeout) {
        try {
            return waitFor(webDriver, ExpectedConditions.presenceOfNestedElementLocatedBy(element, sub_locator), IGNORED_EXCEPTIONS, timeout).isDisplayed();
        } catch (TimeoutException e) {
            return false; // element not found
        }
    }


    public static boolean isFramePresent(WebDriver driver, WebElement element, long timeout) {
        try {
            driver.switchTo().frame(element);
            driver.switchTo().defaultContent();
            return true;
        } catch (NoSuchFrameException | StaleElementReferenceException e) {
            return false; // frame not found
        }
    }


	public static WebElement findElement(WebDriver driver, By by, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfElementLocated(by), IGNORED_EXCEPTIONS, timeout);
	}

	public static WebElement findElement(WebDriver driver, WebElement element, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOf(element), IGNORED_EXCEPTIONS, timeout);
	}

    public static WebElement findElementXpath(WebDriver driver, String xpath, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), IGNORED_EXCEPTIONS, timeout);
    }

    public static List<WebElement> findElementsXpath(WebDriver driver, String xpath, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)), IGNORED_EXCEPTIONS, timeout);
    }

    public static List<WebElement> findElements(WebDriver driver, By by, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(by), IGNORED_EXCEPTIONS, timeout);
    }

    public static List<WebElement> getElements(WebDriver driver, WebElement element, String xpathExpression, long timeout) {
    	if (findElement(driver, element, timeout) != null)
    		return element.findElements(By.xpath(xpathExpression));
    	else 
    		return null;
    }

    public static WebElement getElement(WebDriver driver, WebElement element, String xpathExpression, long timeout) {
    	if (findElement(driver, element, timeout) != null)
    		return element.findElement(By.xpath(xpathExpression));
    	else 
    		return null;
    }
    
    public static void clear(WebElement element) {
        element.clear();
    }

    public static void click(WebElement element, WebDriver driver) {
        // focus element before click when running in browser.
        element.click();
    }
    
    public static String read(WebElement element) {
        String text = null;

        // read the element text
        if ("input".equalsIgnoreCase(element.getTagName())) {
            text = element.getAttribute("value");
        } else {
            text = element.getText();
        }

        // sometime weblement.getText() returns no text while text is
        // present. check innerText in such cases
        if (text == null) {
            text = element.getAttribute("innerText");
        }

        return text;
    }

    public static String readAttribute(WebElement element, String attributeName) {
        String attribValue = element.getAttribute(attributeName);
        return attribValue;
    }

    public static void sendKeys(WebElement element, CharSequence... input) {
        // sendKeys doesn't work for readonly inputs.
        // updated a value attribute in such cases
        if ("input".equals(element.getTagName()) && element.getAttribute("readonly") != null) {
            JavascriptExecutor js = (JavascriptExecutor) ((WrapsDriver) element).getWrappedDriver();
            String script = String.format("arguments[0].setAttribute('value', '%s')", input.toString());
            js.executeScript(script.toString(), element);
        } else {
            element.sendKeys(input);
        }
    }

	public static void sleep(long msec)
	{
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e) {
		}
	}
	
    public static void signatureOnWindow(WebDriver driver, WebElement canvasElement){
    	 Actions actionBuilder=new Actions(driver); 
    	 Action drawOnCanvas = actionBuilder 
    			 // .contextClick(canvasElement) 
    			 .moveToElement(canvasElement)
    			 //canvasElement is the element that holds the signature element you have in the DOM
    			 .clickAndHold() 
    			 .moveByOffset(100, 50)
    			 .moveByOffset(6,7)
    			 .moveByOffset(-15,-50)
    			 .moveByOffset(16,7)
    			 .release()
    			 .build(); 
    	 drawOnCanvas.perform();
    }
    
    public static void setTxtbox(WebDriver driver, WebElement elementInput, String _input){
        click(elementInput, driver);
    	//elementInput.click();
    	elementInput.clear();
        if (!(_input == null || _input.isEmpty())) {
        	elementInput.sendKeys(_input);
        }
    }

	public static void dragAndDrop(WebDriver driver, WebElement sourceElement, WebElement destinationElement) {
		(new Actions(driver)).dragAndDrop(sourceElement, destinationElement).build().perform();
	}
	
	
	//==========================================================================================================================
	// Window handles
	//==========================================================================================================================

	public static String switchToWindow(WebDriver driver, long timeout) {

		ExpectedCondition<Boolean> expectedCondition = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return (driver.getWindowHandles().size() != 1);
			}
		};
		waitFor(driver, expectedCondition, timeout);

		Set<String> handles = driver.getWindowHandles();
		Iterator<String> itr = handles.iterator();
		Object lastHandle = itr.next();
		Object firstHandle = driver.getWindowHandle();
		Object tempHandle;
		while (itr.hasNext()) {
			tempHandle = itr.next();
			if (!tempHandle.toString().equals(firstHandle))
				lastHandle = tempHandle;
		}
		driver.switchTo().window(lastHandle.toString());
		// sometimes print modal was popped up very quickly after another window
		// has been popped up.
		// This will take care of that case.
		waitForLoad(driver, timeout);
		return firstHandle.toString();
	}

	public static void switchToMainWindow(WebDriver driver, String firstHandle, long timeout) {

		ExpectedCondition<Boolean> expectedCondition = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return (driver.getWindowHandles().size() == 1);
			}
		};
		waitFor(driver, expectedCondition, timeout);
		driver.switchTo().window(firstHandle);
	}

	
	public static void getScreenshot(WebDriver driver, String filename) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//==========================================================================================================================
	// Alert handles
	//==========================================================================================================================

	
	public static boolean isAlertPresent(WebDriver driver, long timeout) {
		try {
			Alert alert = waitFor(driver, ExpectedConditions.alertIsPresent(), IGNORED_EXCEPTIONS, timeout);
			if (alert != null) 
				System.out.println("Alert message is [" + alert.getText() + "]");

		    return (alert != null);
		} catch (Exception e) {
			return false;
		}
	}
	
	public static void doAlert(WebDriver driver, String action, long timeout) {
		if (isAlertPresent(driver, timeout)) {
			if ("accept".equalsIgnoreCase(action))
				driver.switchTo().alert().accept();
			else
				driver.switchTo().alert().dismiss();
		}
	}

	
	//==========================================================================================================================
	// Frame handles
	//==========================================================================================================================

	public static boolean isFramePresent(WebDriver driver, String frameLocator, long timeout) {
		try {
			switchFrame(driver, frameLocator, timeout);
			return true;
		} catch (NoSuchFrameException | StaleElementReferenceException e) {
			return false; // frame not found
		}
	}

	// wait for specific frame by name or id
	// this works only for one layer below
	public static WebDriver switchFrame(WebDriver driver, WebElement frame, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame), IGNORED_EXCEPTIONS, timeout);
	}
	
	public static WebDriver switchFrame(WebDriver driver, String frameLocator, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator), IGNORED_EXCEPTIONS, timeout);
	}
	
	public static WebDriver switchFrame(WebDriver driver, int frameLocator, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator), IGNORED_EXCEPTIONS, timeout);
	}

	public static WebDriver switchFrameByXpath(WebDriver driver, String xpath, long timeout) {
		int size = driver.findElements(By.tagName("iframe")).size();
		for(int i=0; i<size; i++){
			driver = switchFrame(driver, i, timeout);
			sleep(1000);
		    if (driver.findElements(By.xpath(xpath)).size() > 0)
		    	break;
		    else
		    	driver = driver.switchTo().defaultContent(); //switching back from the iframe
		}
		
		return driver;
	}
	
	public static List<WebElement> getFramePath(WebDriver driver, String frameName, String frameType) {
		// switch to the top level frame to traverse the frames tree
		driver.switchTo().defaultContent();
		List<WebElement> framePath = findFrames(driver, frameName, frameType);
		Collections.reverse(framePath);
		return framePath;
	}

	private static List<WebElement> findFrames(SearchContext context, String frameName, String frameType) {
		List<WebElement> requiredFramePath = new LinkedList<>();
		List<WebElement> frames = context.findElements(By.tagName(frameType));
		for (WebElement frame : frames) {
			
			if (frameName.equals(frame.getAttribute("id")) || frameName.equals(frame.getAttribute("name")) || frameName.replace("&amp;", "&").equals(frame.getAttribute("src"))) {
				requiredFramePath.add(frame);
			} else {
				requiredFramePath.addAll(findFrames(frame, frameName, frameType));
			}
		}
		return requiredFramePath;
	}

	public static WebDriver switchToFrameByPath(WebDriver driver, List<WebElement> pathToFrame, long timeout) {
		driver = driver.switchTo().defaultContent();
		for (WebElement frame : pathToFrame) {
			driver = switchFrame(driver, frame, timeout);
		}
		
		return driver;
	}
}
