package ch302;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class AnnotationDataProvider {
    @DataProvider(name = "data-provider")
    public Object[][] dataProviderMethod() {
        return new Object[][] { { "one" }, { "two" }, { "three" } };
    }

    @Test(dataProvider = "data-provider")
    public void testMethod(String data) {
        System.out.println("Data is: " + data);
    }
}