package ch302;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AnnotationTest {

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("beforeSuite()");	
	}
	
	@BeforeClass
	public void beforeclass() {
		System.out.println("beforeclass()");
	}
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("beforeTest()");
	}

	@BeforeGroups("ui")
	public void beforeGroups() {
		System.out.println("beforeGroups()");
	}

	@AfterGroups("ui")
	public void afterGroups() {
		System.out.println("afterGroups()");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("beforeMethod()");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("afterMethod()");
	}

	@Test(groups = "ui")
	public void testWeb() {
		System.out.println("testWeb()");
	}

	@Test(groups = "mobile")
	public void testMobile() {
		System.out.println("testMobile()");
	}

	@Test(groups = "api")
	public void testAPI() {
		System.out.println("testAPI()");
	}
	
	@AfterTest
	public void afterTest() {
		System.out.println("afterTest()");
	}
	
	@AfterClass
	public void afterClass() {
		System.out.println("afterClass()");
	}
	
	@AfterSuite
	public void afterSuite() {
		System.out.println("afterSuite()");
	}
}
