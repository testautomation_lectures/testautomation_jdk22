package webdriver_BiDi;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.bidi.module.Network;
import org.openqa.selenium.bidi.network.BeforeRequestSent;
import org.openqa.selenium.bidi.network.ResponseDetails;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

class NetworkEventsTest {

	WebDriver driver;
	WebDriverWait wait;

	@BeforeMethod
	public void setup() {
		FirefoxOptions options = new FirefoxOptions();
		options.setCapability("webSocketUrl", true);
		driver = new FirefoxDriver(options);
	}

	@AfterMethod
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	void canListenToBeforeRequestSentEvent() throws ExecutionException, InterruptedException, TimeoutException {
		try (Network network = new Network(driver)) {
			CompletableFuture<BeforeRequestSent> future = new CompletableFuture<>();
			network.onBeforeRequestSent(future::complete);
			driver.get("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");

			BeforeRequestSent requestSent = future.get(5, TimeUnit.SECONDS);
			String windowHandle = driver.getWindowHandle();
			Assert.assertEquals(windowHandle, requestSent.getBrowsingContextId());
			Assert.assertEquals("get", requestSent.getRequest().getMethod().toLowerCase());
		}
	}

	@Test
	void canListenToResponseStartedEvent() throws ExecutionException, InterruptedException, TimeoutException {
		try (Network network = new Network(driver)) {
			CompletableFuture<ResponseDetails> future = new CompletableFuture<>();
			network.onResponseStarted(future::complete);
			driver.get("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");

			ResponseDetails response = future.get(5, TimeUnit.SECONDS);
			String windowHandle = driver.getWindowHandle();

			Assert.assertEquals(windowHandle, response.getBrowsingContextId());
			Assert.assertEquals("get", response.getRequest().getMethod().toLowerCase());
			Assert.assertEquals(200L, response.getResponseData().getStatus());
		}
	}

	@Test
	void canListenToResponseCompletedEvent() throws ExecutionException, InterruptedException, TimeoutException {
		try (Network network = new Network(driver)) {
			CompletableFuture<ResponseDetails> future = new CompletableFuture<>();
			network.onResponseCompleted(future::complete);
			driver.get("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");

			ResponseDetails response = future.get(5, TimeUnit.SECONDS);
			String windowHandle = driver.getWindowHandle();

			Assert.assertEquals(windowHandle, response.getBrowsingContextId());
			Assert.assertEquals("get", response.getRequest().getMethod().toLowerCase());
			Assert.assertEquals(200L, response.getResponseData().getStatus());
		}
	}

	@Test
	void canListenToResponseCompletedEventWithCookie()
			throws ExecutionException, InterruptedException, TimeoutException {
		try (Network network = new Network(driver)) {
			CompletableFuture<BeforeRequestSent> future = new CompletableFuture<>();

			driver.get("https://www.selenium.dev/selenium/web/blankPage");
			driver.manage().addCookie(new Cookie("foo", "bar"));
			network.onBeforeRequestSent(future::complete);
			driver.navigate().refresh();

			BeforeRequestSent requestSent = future.get(5, TimeUnit.SECONDS);
			String windowHandle = driver.getWindowHandle();

			Assert.assertEquals(windowHandle, requestSent.getBrowsingContextId());
			Assert.assertEquals("get", requestSent.getRequest().getMethod().toLowerCase());

			Assert.assertEquals("foo", requestSent.getRequest().getCookies().get(0).getName());
			Assert.assertEquals("bar", requestSent.getRequest().getCookies().get(0).getValue().getValue());
		}
	}

	@Test
	void canListenToOnAuthRequiredEvent() throws ExecutionException, InterruptedException, TimeoutException {
		try (Network network = new Network(driver)) {
			CompletableFuture<ResponseDetails> future = new CompletableFuture<>();
			network.onAuthRequired(future::complete);
			driver.get("https://the-internet.herokuapp.com/basic_auth");

			ResponseDetails response = future.get(5, TimeUnit.SECONDS);
			String windowHandle = driver.getWindowHandle();
			Assert.assertEquals(windowHandle, response.getBrowsingContextId());
			Assert.assertEquals("get", response.getRequest().getMethod().toLowerCase());
			Assert.assertEquals(401L, response.getResponseData().getStatus());
		}
	}
}
