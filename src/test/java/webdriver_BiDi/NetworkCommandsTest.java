package webdriver_BiDi;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UsernameAndPassword;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.bidi.module.Network;
import org.openqa.selenium.bidi.network.AddInterceptParameters;
import org.openqa.selenium.bidi.network.InterceptPhase;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class NetworkCommandsTest {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeMethod
	public void setup() {
		FirefoxOptions options = new FirefoxOptions();
		options.setCapability("webSocketUrl", true);
		driver = new FirefoxDriver(options);
		wait = new WebDriverWait(driver, Duration.ofSeconds(10));
	}

	@AfterMethod
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test(enabled = false)
	void canAddIntercept() {
		try (Network network = new Network(driver)) {
			String intercept = network.addIntercept(new AddInterceptParameters(InterceptPhase.BEFORE_REQUEST_SENT));
			Assert.assertNotNull(intercept);
		}
	}

	@Test(enabled = false)
	void canRemoveIntercept() {
		try (Network network = new Network(driver)) {
			String intercept = network.addIntercept(new AddInterceptParameters(InterceptPhase.BEFORE_REQUEST_SENT));
			Assert.assertNotNull(intercept);
			network.removeIntercept(intercept);
		}
	}

	@Test(enabled = false)
	void canContinueWithAuthCredentials() {
		try (Network network = new Network(driver)) {
			network.addIntercept(new AddInterceptParameters(InterceptPhase.AUTH_REQUIRED));
			network.onAuthRequired(responseDetails -> network.continueWithAuth(
					responseDetails.getRequest().getRequestId(), new UsernameAndPassword("admin", "admin")));
			driver.get("https://the-internet.herokuapp.com/basic_auth");
			String successMessage = "Congratulations! You must have the proper credentials.";
			WebElement elementMessage = driver.findElement(By.tagName("p"));
			Assert.assertEquals(successMessage, elementMessage.getText());
		}
	}

	@Test(enabled = false)
	void canContinueWithoutAuthCredentials() {
		try (Network network = new Network(driver)) {
			network.addIntercept(new AddInterceptParameters(InterceptPhase.AUTH_REQUIRED));
			network.onAuthRequired(responseDetails ->
			// Does not handle the alert
			network.continueWithAuthNoCredentials(responseDetails.getRequest().getRequestId()));
			driver.get("https://the-internet.herokuapp.com/basic_auth");
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			alert.dismiss();
			Assert.assertTrue(driver.getPageSource().contains("Not authorized"));
		}
	}

	@Test(enabled = false)
	void canCancelAuth() {
		try (Network network = new Network(driver)) {
			network.addIntercept(new AddInterceptParameters(InterceptPhase.AUTH_REQUIRED));
			network.onAuthRequired(responseDetails ->
			// Does not handle the alert
			network.cancelAuth(responseDetails.getRequest().getRequestId()));
			driver.get("https://the-internet.herokuapp.com/basic_auth");
			Assert.assertTrue(driver.getPageSource().contains("Not authorized"));
		}
	}

	@Test(enabled = false)
	void canFailRequest() {
		try (Network network = new Network(driver)) {
			network.addIntercept(new AddInterceptParameters(InterceptPhase.BEFORE_REQUEST_SENT));
			network.onBeforeRequestSent(
					responseDetails -> network.failRequest(responseDetails.getRequest().getRequestId()));
			driver.manage().timeouts().pageLoadTimeout(Duration.of(5, ChronoUnit.SECONDS));
			Assert.assertThrows(TimeoutException.class,
					() -> driver.get("https://the-internet.herokuapp.com/basic_auth"));
		}
	}
}
