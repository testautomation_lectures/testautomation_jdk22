package actionsApi;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.WheelInput;

public class WheelTest {
	@Test
	public void shouldScrollToElement() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get(
					"https://www.selenium.dev/selenium/web/scrolling_tests/frame_with_nested_scrolling_frame_out_of_view.html");

			WebElement iframe = driver.findElement(By.tagName("iframe"));
			new Actions(driver).scrollToElement(iframe).perform();

			Assert.assertTrue(inViewport(iframe, driver));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void shouldScrollFromViewportByGivenAmount() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get(
					"https://www.selenium.dev/selenium/web/scrolling_tests/frame_with_nested_scrolling_frame_out_of_view.html");

			WebElement footer = driver.findElement(By.tagName("footer"));
			int deltaY = footer.getRect().y;
			new Actions(driver).scrollByAmount(0, deltaY).perform();

			Assert.assertTrue(inViewport(footer, driver));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void shouldScrollFromElementByGivenAmount() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get(
					"https://www.selenium.dev/selenium/web/scrolling_tests/frame_with_nested_scrolling_frame_out_of_view.html");

			WebElement iframe = driver.findElement(By.tagName("iframe"));
			WheelInput.ScrollOrigin scrollOrigin = WheelInput.ScrollOrigin.fromElement(iframe);
			new Actions(driver).scrollFromOrigin(scrollOrigin, 0, 200).perform();

			driver.switchTo().frame(iframe);
			WebElement checkbox = driver.findElement(By.name("scroll_checkbox"));
			Assert.assertTrue(inViewport(checkbox, driver));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void shouldScrollFromElementByGivenAmountWithOffset() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get(
					"https://www.selenium.dev/selenium/web/scrolling_tests/frame_with_nested_scrolling_frame_out_of_view.html");

			WebElement footer = driver.findElement(By.tagName("footer"));
			WheelInput.ScrollOrigin scrollOrigin = WheelInput.ScrollOrigin.fromElement(footer, 0, -50);
			new Actions(driver).scrollFromOrigin(scrollOrigin, 0, 200).perform();

			WebElement iframe = driver.findElement(By.tagName("iframe"));
			driver.switchTo().frame(iframe);
			WebElement checkbox = driver.findElement(By.name("scroll_checkbox"));
			Assert.assertTrue(inViewport(checkbox, driver));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void shouldScrollFromViewportByGivenAmountFromOrigin() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/scrolling_tests/frame_with_nested_scrolling_frame.html");

			WheelInput.ScrollOrigin scrollOrigin = WheelInput.ScrollOrigin.fromViewport(10, 10);
			new Actions(driver).scrollFromOrigin(scrollOrigin, 0, 200).perform();

			WebElement iframe = driver.findElement(By.tagName("iframe"));
			driver.switchTo().frame(iframe);
			WebElement checkbox = driver.findElement(By.name("scroll_checkbox"));
			Assert.assertTrue(inViewport(checkbox, driver));
		} finally {
			driver.quit();
		}
	}

	private boolean inViewport(WebElement element, WebDriver driver) {

		String script = "for(var e=arguments[0],f=e.offsetTop,t=e.offsetLeft,o=e.offsetWidth,n=e.offsetHeight;\n"
				+ "e.offsetParent;)f+=(e=e.offsetParent).offsetTop,t+=e.offsetLeft;\n"
				+ "return f<window.pageYOffset+window.innerHeight&&t<window.pageXOffset+window.innerWidth&&f+n>\n"
				+ "window.pageYOffset&&t+o>window.pageXOffset";

		return (boolean) ((JavascriptExecutor) driver).executeScript(script, element);
	}
}
