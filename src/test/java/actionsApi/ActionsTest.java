package actionsApi;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.time.Duration;

public class ActionsTest {
	@Test
	public void pause() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/mouse_interaction.html");

			long start = System.currentTimeMillis();

			WebElement clickable = driver.findElement(By.id("clickable"));
			new Actions(driver)
					.moveToElement(clickable)
					.pause(Duration.ofSeconds(1))
					.clickAndHold()
					.pause(Duration.ofSeconds(1))
					.sendKeys("abc")
					.perform();

			long duration = System.currentTimeMillis() - start;
			Assert.assertTrue(duration > 2000);
			Assert.assertTrue(duration < 3000);
		} finally {
			driver.quit();
		}
	}

	@Test
	public void releasesAll() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/mouse_interaction.html");

			WebElement clickable = driver.findElement(By.id("clickable"));
			Actions actions = new Actions(driver);
			actions.clickAndHold(clickable)
				.keyDown(Keys.SHIFT)
				.sendKeys("a")
				.perform();

			((RemoteWebDriver) driver).resetInputState();

			actions.sendKeys("a").perform();
			Assert.assertEquals("A", String.valueOf(clickable.getAttribute("value").charAt(0)));
			Assert.assertEquals("a", String.valueOf(clickable.getAttribute("value").charAt(1)));
		} finally {
			driver.quit();
		}
	}
}
