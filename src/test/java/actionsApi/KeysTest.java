package actionsApi;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class KeysTest {
	@Test
	public void keyDown() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/single_text_input.html");

			new Actions(driver).keyDown(Keys.SHIFT).sendKeys("a").perform();

			WebElement textField = driver.findElement(By.id("textInput"));
			Assert.assertEquals("A", textField.getAttribute("value"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void keyUp() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/single_text_input.html");

			new Actions(driver).keyDown(Keys.SHIFT).sendKeys("a").keyUp(Keys.SHIFT).sendKeys("b").perform();

			WebElement textField = driver.findElement(By.id("textInput"));
			Assert.assertEquals("Ab", textField.getAttribute("value"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void sendKeysToActiveElement() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/single_text_input.html");

			new Actions(driver).sendKeys("abc").perform();

			WebElement textField = driver.findElement(By.id("textInput"));
			Assert.assertEquals("abc", textField.getAttribute("value"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void sendKeysToDesignatedElement() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/single_text_input.html");
			driver.findElement(By.tagName("body")).click();

			WebElement textField = driver.findElement(By.id("textInput"));
			new Actions(driver).sendKeys(textField, "Selenium!").perform();

			Assert.assertEquals("Selenium!", textField.getAttribute("value"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void copyAndPaste() {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev/selenium/web/single_text_input.html");

			Keys cmdCtrl = Platform.getCurrent().is(Platform.MAC) ? Keys.COMMAND : Keys.CONTROL;

			WebElement textField = driver.findElement(By.id("textInput"));
			new Actions(driver).sendKeys(textField, "Selenium!").sendKeys(Keys.ARROW_LEFT).keyDown(Keys.SHIFT)
					.sendKeys(Keys.ARROW_UP).keyUp(Keys.SHIFT).keyDown(cmdCtrl).sendKeys("xvv").keyUp(cmdCtrl)
					.perform();

			Assert.assertEquals("SeleniumSelenium!", textField.getAttribute("value"));
		} finally {
			driver.quit();
		}
	}
}
