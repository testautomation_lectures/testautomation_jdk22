package ch307;

import org.testng.annotations.Test;

public class ParallelTest2 {
  
	@Test(dependsOnMethods = { "testB" })
	public void testA() {
		long id = Thread.currentThread().getId();
		System.out.println("testA(). Thread id is: " + id);
	}

	@Test()
	public void testB() {
		long id = Thread.currentThread().getId();
		System.out.println("testB(). Thread id is: " + id);
	}

	@Test(dependsOnMethods = { "testB" })
	public void testC() {
		long id = Thread.currentThread().getId();
		System.out.println("testC(). Thread id is: " + id);
	}
}