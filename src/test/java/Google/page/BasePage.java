package Google.page;

import org.openqa.selenium.WebDriver;

public class BasePage {
	protected WebDriver driver;
	protected long timeout = 60;

	public BasePage(WebDriver driver) {
		this.driver = driver;
	}
}
