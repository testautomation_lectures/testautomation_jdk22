package Google.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import common.SeleniumWebUtils;

public class GoogleVersion extends BasePage {

	private WebElement el;

	public GoogleVersion(WebDriver driver) {
		super(driver);

		if (!isDisplayed()) {
			throw new IllegalStateException(this.getClass().getName() + " is not loaded");
		}
		el = SeleniumWebUtils.findElement(driver, By.id("version"), timeout);
	}

	public boolean isDisplayed() {
		return SeleniumWebUtils.isDisplayed(driver, By.id("version"), timeout);
	}
	
	public String getVersion() {
		return SeleniumWebUtils.read(el);
	}
}
