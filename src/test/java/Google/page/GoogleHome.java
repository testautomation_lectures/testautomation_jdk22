package Google.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import common.SeleniumWebUtils;

public class GoogleHome {
	protected WebDriver driver;

	public WebElement searchBox;
	public WebElement signInButton;

	private long timeout = 60;

	public GoogleHome(WebDriver driver) {
		this.driver = driver;

		if (!isDisplayed()) {
			throw new IllegalStateException(this.getClass().getName() + " is not loaded");
		}
		searchBox = SeleniumWebUtils.findElement(driver, By.xpath("//textarea"), timeout);
		signInButton = SeleniumWebUtils.findElement(driver, By.xpath("//a[.='로그인']"), timeout);
	}

	public boolean isDisplayed() {
		return SeleniumWebUtils.isDisplayed(driver, By.xpath("//textarea"), timeout);
	}
}
