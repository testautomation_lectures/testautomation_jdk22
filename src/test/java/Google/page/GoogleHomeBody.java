package Google.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import common.SeleniumWebUtils;

public class GoogleHomeBody extends BasePage{

	private WebElement searchBox;

	public GoogleHomeBody(WebDriver driver) {
		super(driver);

		if (!isDisplayed()) {
			throw new IllegalStateException(this.getClass().getName() + " is not loaded");
		}
		searchBox = SeleniumWebUtils.findElement(driver, By.xpath("//textarea"), timeout);
	}

	public boolean isDisplayed() {
		return SeleniumWebUtils.isDisplayed(this.driver, By.xpath("//textarea"), timeout);
	}
	
	public void putKeysInSearchBox(CharSequence... keysToSend) {
		searchBox.sendKeys(keysToSend);
	}
}
