package Google.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import common.SeleniumWebUtils;

public class GoogleHomeHeader extends BasePage {
	private WebElement signInButton;

	public GoogleHomeHeader(WebDriver driver) {
		super(driver);

		if (!isDisplayed()) {
			throw new IllegalStateException(this.getClass().getName() + " is not loaded");
		}
		signInButton = SeleniumWebUtils.findElement(driver, By.xpath("//a[.='로그인']"), timeout);
	}

	public boolean isDisplayed() {
		return SeleniumWebUtils.isDisplayed(this.driver, By.xpath("//a[.='로그인']"), timeout);
	}
	
	public String getSignInBtnText() {
		return SeleniumWebUtils.read(signInButton);
	}
}
