package Google.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import common.SeleniumWebUtils;

public class GoogleResult {
	protected WebDriver driver;

    private WebElement signInButton;
    private WebElement searchRelated;

	private long timeout = 60;
	
    public GoogleResult(WebDriver driver) {
		this.driver = driver;
        
		if (!isDisplayed()) {
			throw new IllegalStateException(this.getClass().getName() + " is not loaded");
		}
		signInButton = SeleniumWebUtils.findElement(driver, By.xpath("//a[.='로그인']"), timeout);
		searchRelated = SeleniumWebUtils.findElement(driver, By.xpath("//div[@data-attrid='title']"), timeout);
		
    }
    
	public boolean isDisplayed() {
		return SeleniumWebUtils.isDisplayed(driver, By.xpath("//a[.='로그인']"), timeout);
	}
	
	public String getSignInBtnText() {
		return SeleniumWebUtils.read(signInButton);
	}
	
	public String getSearchPanelText() {
		return SeleniumWebUtils.read(searchRelated);
	}
}
