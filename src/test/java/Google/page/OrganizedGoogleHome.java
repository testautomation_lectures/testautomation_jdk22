package Google.page;

import org.openqa.selenium.WebDriver;

public class OrganizedGoogleHome extends BasePage {
	private GoogleHomeHeader googlehomeheader;
	private GoogleHomeBody googlehomebody;
	
	public OrganizedGoogleHome(WebDriver driver) {
		super(driver);

		googlehomeheader = new GoogleHomeHeader(driver);
		googlehomebody = new GoogleHomeBody(driver);
		if (!isDisplayed()) {
			throw new IllegalStateException(this.getClass().getName() + " is not loaded");
		}
	}

	public GoogleHomeHeader getHeader() {
		return googlehomeheader;
	}
	public GoogleHomeBody getBody() {
		return googlehomebody;
	}
	public boolean isDisplayed() {
		return googlehomeheader.isDisplayed() && googlehomebody.isDisplayed();
	}
}
