package Google.test;

import Google.page.GoogleHome;
import Google.page.GoogleResult;
import Google.page.GoogleVersion;
import common.SeleniumWebUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GoogleHomeTest {
	WebDriver driver;

	@BeforeMethod
	public void setup() {
		driver = new ChromeDriver();
	}

	@AfterMethod
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}
	
	
    @Test()
    public void searchAndNavigateWeb(){
        String targetUrl= "https://google.com";
        String textToSend = "this is a test";
        String signInLinkText = "로그인";

        driver.get(targetUrl);
        GoogleHome googlehome = new GoogleHome(driver);
        Assert.assertEquals(googlehome.signInButton.getText(), signInLinkText);
        System.out.println("Navigated successfully to the Google search page");
        SeleniumWebUtils.getScreenshot(driver, ".\\googlehometest1.png");
        googlehome.searchBox.sendKeys(textToSend);
        SeleniumWebUtils.sleep(3000L);
        SeleniumWebUtils.getScreenshot(driver, ".\\googlehometest2.png");
        googlehome.searchBox.sendKeys(Keys.RETURN);
        //---------------- navigate Search Result page
        GoogleResult googleresult = new GoogleResult(driver);
        
        Assert.assertEquals(googleresult.getSignInBtnText(), signInLinkText);
        System.out.println("Found Sign-in button");
        SeleniumWebUtils.getScreenshot(driver, ".\\googlehometest3.png");
        Assert.assertTrue(driver.getPageSource().toLowerCase().contains(textToSend));
        System.out.println("Validating search page success");
        SeleniumWebUtils.sleep(3000L);
    }

    @Test()
    public void browserVersion(){
        driver.get("chrome://version");
        GoogleVersion googleVersion = new GoogleVersion(driver);
        System.out.println("version:" + googleVersion.getVersion());
        SeleniumWebUtils.getScreenshot(driver, ".\\browserVersion.png");

        Assert.assertNotNull(googleVersion.getVersion());
        SeleniumWebUtils.sleep(3000L);
    }

}
