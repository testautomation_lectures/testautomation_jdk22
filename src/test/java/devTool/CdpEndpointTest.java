package devTool;

import com.google.common.collect.ImmutableMap;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chromium.HasCdp;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CdpEndpointTest {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeMethod
	public void createSession() {
		driver = new ChromeDriver();
	}

	@AfterMethod
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void setCookie() {
		Map<String, Object> cookie = new HashMap<>();
		cookie.put("name", "cheese");
		cookie.put("value", "gouda");
		cookie.put("domain", "www.selenium.dev");
		cookie.put("secure", true);

		((HasCdp) driver).executeCdpCommand("Network.setCookie", cookie);

		driver.get("https://www.selenium.dev");
		Cookie cheese = driver.manage().getCookieNamed("cheese");
		Assert.assertEquals("gouda", cheese.getValue());
	}

	@Test
	public void performanceMetrics() {
		driver.get("https://www.selenium.dev/selenium/web/frameset.html");

		((HasCdp) driver).executeCdpCommand("Performance.enable", new HashMap<>());

		Map<String, Object> response = ((HasCdp) driver).executeCdpCommand("Performance.getMetrics", new HashMap<>());
		List<Map<String, Object>> metricList = (List<Map<String, Object>>) response.get("metrics");

		Map<String, Number> metrics = new HashMap<>();
		System.out.println("performance Metrics");
		for (Map<String, Object> metric : metricList) {
			metrics.put((String) metric.get("name"), (Number) metric.get("value"));
			System.out.println(metric.get("name").toString()+" : "+metric.get("value").toString());
		}

		Assert.assertTrue(metrics.get("DevToolsCommandDuration").doubleValue() > 0);
		Assert.assertEquals(12, metrics.get("Frames").intValue());
	}

	@Test
	public void basicAuth() {
		((HasCdp) driver).executeCdpCommand("Network.enable", new HashMap<>());

		String encodedAuth = Base64.getEncoder().encodeToString("admin:admin".getBytes());
		Map<String, Object> headers = ImmutableMap.of("headers",
				ImmutableMap.of("authorization", "Basic " + encodedAuth));

		((HasCdp) driver).executeCdpCommand("Network.setExtraHTTPHeaders", headers);

		driver.get("https://the-internet.herokuapp.com/basic_auth");

		Assert.assertEquals("Congratulations! You must have the proper credentials.",
				driver.findElement(By.tagName("p")).getText());
	}

	@Test
	public void MockGeoLocation() {
		Map<String, Object> geoloc = new HashMap<>();
		/*
		 * //New York city 
		 * geoloc.put("latitude", 40.7128); 
		 * geoloc.put("longitude",-74.0060); 
		 * geoloc.put("accuracy", 100);
		 */

		// Washington DC
		geoloc.put("latitude", 38.9072);
		geoloc.put("longitude", -77.0369);
		geoloc.put("accuracy", 100);
		((ChromeDriver) driver).executeCdpCommand("Emulation.setGeolocationOverride", geoloc);
		driver.get("https://locations.kfc.com/search");
		WebElement elem = driver.findElement(By.cssSelector(".Locator-button.js-locator-geolocateTrigger"));
		elem.click();
	}

}
