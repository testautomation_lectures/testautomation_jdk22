package ch305;

import org.testng.annotations.Test;

public class ExpectedException {

	@Test(expectedExceptions = ArithmeticException.class)
	public void testDividedByZero() {
		System.out.println("a = 0");
		System.out.println("b = 1/0");
		int a = 0;
		int b = 1 / a;
	}
}