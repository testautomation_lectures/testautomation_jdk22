package browserInteractions;

import java.time.Duration;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AlertsTest{
	@Test
	public void simpleAlert() {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("https://www.selenium.dev/documentation/webdriver/interactions/alerts/");
			
			//Click the link to activate the alert
			driver.findElement(By.linkText("See an example alert")).click();

			//Wait for the alert to be displayed and store it in a variable
			Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(2));
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());

			//Store the alert text in a variable
			String text = alert.getText();

			//Press the OK button
			alert.accept();			
			Assert.assertEquals(text, "Sample alert");
			
		} finally {
			driver.quit();
		}
	}

	@Test
	public void confirmAlert() {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("https://www.selenium.dev/documentation/webdriver/interactions/alerts/");
			
			//Click the link to activate the alert
			driver.findElement(By.linkText("See a sample confirm")).click();

			//Wait for the alert to be displayed and store it in a variable
			Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(2));
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());

			//Store the alert in a variable for reuse
			String text = alert.getText();

			//Press the Cancel button
			alert.dismiss();
			Assert.assertEquals(text, "Are you sure?");
			
		} finally {
			driver.quit();
		}
	}

	@Test
	public void promptAlert() {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("https://www.selenium.dev/documentation/webdriver/interactions/alerts/");
			
			//Click the link to activate the alert
			driver.findElement(By.linkText("See a sample prompt")).click();

			//Wait for the alert to be displayed and store it in a variable
			Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(2));
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());

			//Type your message
			alert.sendKeys("Selenium");

			//Press the OK button
			alert.accept();			
			
		} finally {
			driver.quit();
		}
	}

}
