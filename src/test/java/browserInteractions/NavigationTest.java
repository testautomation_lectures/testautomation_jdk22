package browserInteractions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NavigationTest {
	@Test
	public void navigateBrowser() {

		WebDriver driver = new ChromeDriver();

		// Convenient
		driver.get("https://selenium.dev");

		// Longer way
		driver.navigate().to("https://selenium.dev");
		String title = driver.getTitle();
		Assert.assertEquals(title, "Selenium");

		// get current url
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url, "https://www.selenium.dev/");

		// Back
		driver.navigate().back();
		title = driver.getTitle();
		Assert.assertEquals(title, "Selenium");

		// Forward
		driver.navigate().forward();
		title = driver.getTitle();
		Assert.assertEquals(title, "Selenium");

		// Refresh
		driver.navigate().refresh();
		title = driver.getTitle();
		Assert.assertEquals(title, "Selenium");

		driver.quit();
	}
}
