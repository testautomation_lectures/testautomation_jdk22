package browserInteractions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Base64;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.print.PrintOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.apache.commons.io.FileUtils;

public class WindowsTest {
	@Test
	public void windowHandle() {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("https://www.selenium.dev/documentation/webdriver/interactions/windows/");

			// Store the ID of the original window
			String originalWindow = driver.getWindowHandle();

			// Check we don't have other windows open already
			Assert.assertTrue(driver.getWindowHandles().size() == 1);

			// Click the link which opens in a new window
			driver.findElement(By.linkText("new window")).click();

			// Wait for the new window or tab
			Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(5));
			wait.until(ExpectedConditions.numberOfWindowsToBe(2));

			// Loop through until we find a new window handle
			for (String windowHandle : driver.getWindowHandles()) {
				if (!originalWindow.contentEquals(windowHandle)) {
					driver.switchTo().window(windowHandle);
					break;
				}
			}

			// Wait for the new tab to finish loading content
			wait.until(ExpectedConditions.titleIs("Selenium"));

		} finally {
			driver.quit();
		}
	}

	@Test
	public void WindowMoveSize() {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("https://www.selenium.dev/documentation/webdriver/interactions/windows/");

			// Store the ID of the original window
			String originalWindow = driver.getWindowHandle();

			driver.manage().window().setSize(new Dimension(1024, 768));
			Dimension wsize = driver.manage().window().getSize();
			Assert.assertEquals(wsize, new Dimension(1024, 768));

			// Move the window to the top left of the primary monitor
			driver.manage().window().setPosition(new Point(100, 0));
			Point wposition = driver.manage().window().getPosition();
			Assert.assertEquals(wposition, new Point(100, 0));

			// minimize window
			driver.manage().window().minimize();
			// maximize window
			driver.manage().window().maximize();

			// full screen mode
			driver.manage().window().fullscreen();

		} finally {
			driver.quit();
		}
	}

	@Test
	public void Screenshot() throws IOException {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("http://www.example.com");
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("./image.png"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void ElementTakeScreenshot() throws IOException {
		WebDriver driver = new ChromeDriver();

		try {
			driver.get("http://www.example.com");
			WebElement element = driver.findElement(By.cssSelector("h1"));
			File scrFile = element.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("./elementImage.png"));
		} finally {
			driver.quit();
		}
	}

	@Test
	public void prints() throws IOException {
		WebDriver driver = new ChromeDriver();
		try {
			driver.get("https://www.selenium.dev");

			String content = ((RemoteWebDriver) driver).print(new PrintOptions()).getContent();
			byte[] bytes = Base64.getDecoder().decode(content);
			Files.write(Paths.get("selenium.pdf"), bytes);
		} finally {
			driver.quit();
		}
	}
}
