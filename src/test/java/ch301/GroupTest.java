package ch301;

import org.testng.annotations.Test;

public class GroupTest {
  
	@Test(groups = "ui")
	public void testWeb() {
		System.out.println("testWeb()");
	}

	@Test(groups = "mobile")
	public void testMobile() {
		System.out.println("testMobile()");
	}

	@Test(groups = "api")
	public void testAPI() {
		System.out.println("testAPI()");
	}
}