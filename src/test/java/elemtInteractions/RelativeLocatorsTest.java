package elemtInteractions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.testng.annotations.Test;

public class RelativeLocatorsTest {

	@Test
	public void relativeLocatorsAbove() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launching a browser and navigate to a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify first element
			WebElement l = driver.findElement(By.xpath("//*[@id='collapseTwo']/div/ul/li[2]/a"));

			// identify element above the first element
			WebElement e = driver.findElement(RelativeLocator.with(By.tagName("a")).above(l));

			// Getting element text value the above identified element
			System.out.println("Getting element text: " + e.getText());

		} finally {
			driver.quit();
		}
	}

	@Test
	public void relativeLocatorsBelow() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launching a browser and navigate to a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify first element
			WebElement l = driver.findElement(By.xpath("//*[@id='collapseTwo']/div/ul/li[2]/a"));

			// identify element below the first element
			WebElement e = driver.findElement(RelativeLocator.with(By.tagName("a")).below(l));

			// Getting element text value the below identified element
			System.out.println("Getting element text: " + e.getText());

		} finally {
			driver.quit();
		}
	}

	@Test
	public void relativeLocatorsLeft() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launching a browser and navigate to a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify first element
			WebElement l = driver.findElement(By.xpath("//*[@id='name']"));

			// identify element left of the first element
			WebElement e = driver.findElement(RelativeLocator.with(By.tagName("label")).toLeftOf(l));

			// Getting element text to left of identified element
			System.out.println("Getting element text: " + e.getText());

		} finally {
			driver.quit();
		}
	}

	@Test
	public void relativeLocatorsRight() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launching a browser and navigate to a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify first element
			WebElement l = driver.findElement(By.xpath("/html/body/div/header/div[1]/a"));

			// identify element right of the first element
			WebElement e = driver.findElement(RelativeLocator.with(By.tagName("h1")).toRightOf(l));

			// Getting element text to right of identified element
			System.out.println("Getting element text: " + e.getText());

		} finally {
			driver.quit();
		}
	}

	@Test
	public void relativeLocatorsNear() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launching a browser and navigate to a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify first element
			WebElement l = driver.findElement(By.xpath("//*[@id='practiceForm']/div[7]/div/div/div[1]/label"));

			// identify element near the first element
			WebElement e = driver.findElement(RelativeLocator.with(By.tagName("input")).near(l));

			// check checkbox
			e.click();

			// verify is selected
			System.out.println("Verify if selected: " + e.isSelected());

		} finally {
			driver.quit();
		}
	}

	@Test
	public void relativeLocatorsChain() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launching a browser and navigate to a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify first element
			WebElement l = driver.findElement(By.xpath("//*[@id='practiceForm']/div[1]/label"));

			// identify second element
			WebElement s = driver.findElement(By.xpath("//*[@id='practiceForm']/div[2]/label"));

			// identify element by chaining elements
			WebElement e = driver.findElement(RelativeLocator.with(By.tagName("input")).above(s).toRightOf(l));

			// input text
			e.sendKeys("Selenium");

			// verify is selected
			System.out.println("Value entered is: " + e.getAttribute("value"));

		} finally {
			driver.quit();
		}
	}

}
