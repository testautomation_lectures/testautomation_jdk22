package elemtInteractions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class LocatorsTest {

	@Test
	public void locatoerID() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// Opening the webpage where we will identify edit box enter text
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// Identify the search box with id locator then enter text Selenium
			WebElement i = driver.findElement(By.id("name"));
			i.sendKeys("Selenium");

			// Get the value entered
			String text = i.getAttribute("value");
			System.out.println("Entered text is: " + text);

		} finally {
			driver.quit();
		}
	}

	@Test
	public void locatoerName() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// Opening the webpage where we will identify edit box enter text
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// Identify the search box with name locator to enter text
			WebElement i = driver.findElement(By.name("name"));
			i.sendKeys("Selenium");

			// Get the value entered
			String text = i.getAttribute("value");
			System.out.println("Entered text is: " + text);

		} finally {
			driver.quit();
		}
	}

	@Test
	public void locatoerClassName() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// Opening the webpage where we will identify button to click
			driver.get("https://www.tutorialspoint.com/selenium/practice/buttons.php");

			// Identify button with class name to click
			WebElement i = driver.findElement(By.className("btn-primary"));
			i.click();

			// Get text after click
			WebElement e = driver.findElement(By.xpath("//*[@id='welcomeDiv']"));
			String text = e.getText();
			System.out.println("Text is: " + text);

		} finally {
			driver.quit();
		}
	}

	@Test
	public void locatoerTagName() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launch a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// Identify input box with tagname locator
			WebElement t = driver.findElement(By.tagName("input"));

			// then enter text
			t.sendKeys("Selenium");

			// Get the value entered
			String text = t.getAttribute("value");
			System.out.println("Entered text is: " + text);

		} finally {
			driver.quit();
		}
	}

	@Test
	public void locatoerLinkText() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launch a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/links.php");

			// Identify link with tagname link text
			WebElement t = driver.findElement(By.linkText("Created"));

			// then click
			t.click();

			// Get the text
			WebElement e = driver.findElement(By.xpath("/html/body/main/div/div/div[2]/div[1]"));
			String text = e.getText();
			System.out.println("Text is: " + text);

		} finally {
			driver.quit();
		}
	}

	@Test
	public void locatoerPartialLinkText() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// launch a URL
			driver.get("https://www.tutorialspoint.com/selenium/practice/links.php");

			// Identify link with tagname partial link text
			WebElement t = driver.findElement(By.partialLinkText("Bad"));

			// then click
			t.click();

			// Get the text
			WebElement e = driver.findElement(By.xpath("/html/body/main/div/div/div[2]/div[4]"));
			String text = e.getText();
			System.out.println("Text is: " + text);

		} finally {
			driver.quit();
		}
	}

}
