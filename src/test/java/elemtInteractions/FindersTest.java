package elemtInteractions;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class FindersTest {

	@Test
	public void singleFinder() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// Opening the webpage
			driver.get("https://www.tutorialspoint.com/selenium/practice/selenium_automation_practice.php");

			// identify element then click
			WebElement l = driver.findElement(By.xpath("//*[@id='headingOne']/button"));
			l.click();

			// identify all elements under tagname ul
			WebElement elements = driver.findElement(By.id("navMenus"));

			// identify first li element under ul then click
			WebElement element = elements.findElement(By.xpath("//li[1]/a"));
			element.click();

			WebElement e = driver.findElement(By.xpath("//*[@id='TextForm']/h1"));
			System.out.println("Text is: " + e.getText());

		} finally {
			driver.quit();
		}
	}

	@Test
	public void multipleFinder() {
		WebDriver driver = new ChromeDriver();
		try {
			// adding implicit wait of 10 secs
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

			// Opening the webpage where we will count the links
			driver.get("https://www.tutorialspoint.com/selenium/practice/links.php");

			// Retrieve all links using locator By.tagName and storing in List
			List<WebElement> totalLnks = driver.findElements(By.tagName("a"));
			System.out.println("Total number of links: " + totalLnks.size());

			// Running loop through list of web elements
			for (int j = 0; j < totalLnks.size(); j++) {
				if (totalLnks.get(j).getText().equalsIgnoreCase("Bad Request")) {
					totalLnks.get(j).click();
					break;
				}
			}

			// get text
			WebElement t = driver.findElement(By.xpath("/html/body/main/div/div/div[2]/div[4]"));
			System.out.println("Text is: " + t.getText());

		} finally {
			driver.quit();
		}
	}

}
