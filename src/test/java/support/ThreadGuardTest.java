package support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ThreadGuard;
import org.testng.annotations.Test;

public class ThreadGuardTest {

	// thread main (id 1) created this driver
	private WebDriver protectedDriver = ThreadGuard.protect(new ChromeDriver());

	// Thread-1 (id 24) is calling the same driver causing the clash to happen
	Runnable r1 = () -> {
		protectedDriver.get("https://selenium.dev");
	};
	Thread thr1 = new Thread(r1);

	void runThreads() {
		thr1.start();
	}

	@Test
	public void driverClash() {
		new ThreadGuardTest().runThreads();
	}
}
