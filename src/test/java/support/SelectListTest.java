package support;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class SelectListTest {
	WebDriver driver;

	@BeforeMethod
	public void navigate() {
		driver = new ChromeDriver();
		driver.get("https://www.selenium.dev/selenium/web/formPage.html");
	}

	@AfterMethod
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void selectOption() {
		WebElement selectElement = driver.findElement(By.name("selectomatic"));
		Select select = new Select(selectElement);

		WebElement twoElement = driver.findElement(By.cssSelector("option[value=two]"));
		WebElement fourElement = driver.findElement(By.cssSelector("option[value=four]"));
		WebElement countElement = driver
				.findElement(By.cssSelector("option[value='still learning how to count, apparently']"));

		select.selectByVisibleText("Four");
		Assert.assertTrue(fourElement.isSelected());

		select.selectByValue("two");
		Assert.assertTrue(twoElement.isSelected());

		select.selectByIndex(3);
		Assert.assertTrue(countElement.isSelected());
	}

	@Test
	public void selectMultipleOption() {
		WebElement selectElement = driver.findElement(By.name("multi"));
		Select select = new Select(selectElement);

		WebElement hamElement = driver.findElement(By.cssSelector("option[value=ham]"));
		WebElement gravyElement = driver.findElement(By.cssSelector("option[value='onion gravy']"));
		WebElement eggElement = driver.findElement(By.cssSelector("option[value=eggs]"));
		WebElement sausageElement = driver.findElement(By.cssSelector("option[value='sausages']"));

		List<WebElement> optionElements = selectElement.findElements(By.tagName("option"));
		List<WebElement> optionList = select.getOptions();
		Assert.assertEquals(optionElements, optionList);

		List<WebElement> selectedOptionList = select.getAllSelectedOptions();
		List<WebElement> expectedSelection = new ArrayList<WebElement>() {
			{
				add(eggElement);
				add(sausageElement);
			}
		};
		Assert.assertEquals(expectedSelection, selectedOptionList);

		select.selectByValue("ham");
		select.selectByValue("onion gravy");
		Assert.assertTrue(hamElement.isSelected());
		Assert.assertTrue(gravyElement.isSelected());

		select.deselectByValue("eggs");
		select.deselectByValue("sausages");
		Assert.assertFalse(eggElement.isSelected());
		Assert.assertFalse(sausageElement.isSelected());
	}

	@Test
	public void disabledOption() {
		WebElement selectElement = driver.findElement(By.name("single_disabled"));
		Select select = new Select(selectElement);

		Assert.assertThrows(UnsupportedOperationException.class, () -> {
			select.selectByValue("disabled");
		});
	}
}
