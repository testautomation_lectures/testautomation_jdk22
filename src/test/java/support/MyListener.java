package support;


import java.lang.reflect.Method;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.WebDriverListener;

public class MyListener implements WebDriverListener  {

    @Override
    public void beforeGet(WebDriver driver, String url) {
    	System.out.println("About to open a page "+ url);
    }
    @Override
    public void afterGetText(WebElement element, String result) {
    	System.out.println("Element "+element+" has text '"+result+"'");
    }	

    @Override
    public void beforeAnyWebElementCall(WebElement element, Method method, Object[] args) {
      System.out.println("About to call a method "+method+" in element "+element+" with parameters "+ args);
    }
    @Override
    public void afterAnyWebElementCall(WebElement element, Method method, Object[] args, Object result) {
    	System.out.println("Method "+method+" called in element "+element+" with parameters "+args+" returned "+ result);
    }
}
