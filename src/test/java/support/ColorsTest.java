package support;

import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ColorsTest {

	private final Color RGBA_WHITE = Color.fromString("rgba(255, 255, 255, 1)");
	
	  @Test
	  public void ReadColor() {
	    ChromeOptions chromeOptions = new ChromeOptions();
	    chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
	      // Navigate to Url
	      driver.get("https://selenium.dev");
	      Color bannerColor = Color.fromString(driver.findElement(By.xpath("/html/body/div/main/section[1]")).getCssValue("color"));
	      Assert.assertTrue(bannerColor.equals(RGBA_WHITE));
	    } finally {
	      driver.quit();
	    }
	  }
}
