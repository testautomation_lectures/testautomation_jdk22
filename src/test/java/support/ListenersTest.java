package support;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringDecorator;

public class ListenersTest {

	public static final String USERNAME = "prakharkulshresh2";
	public static final String AUTOMATE_KEY = "dphXto2bxDtgA3DsVRVy";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	@Test
	public void myListenertest() {
		WebDriver original = new ChromeDriver();
		MyListener listener = new MyListener();
		WebDriver decorated = new EventFiringDecorator(listener).decorate(original);
		decorated.get("http://www.google.com");
		WebElement element = decorated.findElement(By.name("q"));
		element.sendKeys("BrowserStack");
		element.submit();
		System.out.println(decorated.getTitle());
		decorated.quit();
	}
}
