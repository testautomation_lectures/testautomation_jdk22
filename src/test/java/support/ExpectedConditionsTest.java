package support;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class ExpectedConditionsTest {

	private static final List<Class<? extends Exception>> IGNORED_EXCEPTIONS = new ArrayList<>();
	static {
		IGNORED_EXCEPTIONS.add(StaleElementReferenceException.class);
	}
	WebDriver driver;
	WebDriverWait wait;

	@BeforeMethod
	public void setup() {
		FirefoxOptions options = new FirefoxOptions();
		options.setCapability("webSocketUrl", true);
		driver = new FirefoxDriver(options);
	}

	@AfterMethod
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	private static final int POLLING_TIME = 1000;

	@Test
	public void handleAnotherWindow() {
		
		//Set timeout value to 10 seconds as maximum
		long timeout = 10L;
		
		//Maximize window
		driver.manage().window().maximize();
		//Open Browser
		driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_win_open");
		
		//switch to iframe
		List<WebElement> pathToFrame = getFramePath(driver, "iframeResult", "iframe");
        driver = switchToFrameByPath(driver, pathToFrame, 3L);
        		
		//find the button to open new window
        WebElement element = findElementXpath(driver, "/html/body/button", timeout);
        sleep(3000);
        click(element, driver);
        sleep(3000);
	}	
	
	
	
	
	private <T> T waitFor(WebDriver webDriver, ExpectedCondition<T> waitCondition, long timeout) {
		return new WebDriverWait(webDriver, Duration.ofMillis(timeout), Duration.ofMillis(POLLING_TIME)).until(waitCondition);
	}

	/**
	 * Waits for the specified condition until default timeout, ignoring exception
	 */
	private <T> T waitFor(WebDriver webDriver, ExpectedCondition<T> waitCondition,
			List<Class<? extends Exception>> exceptionsToIgnore, long timeout) {
		return new WebDriverWait(webDriver, Duration.ofMillis(timeout), Duration.ofMillis(POLLING_TIME)).ignoreAll(exceptionsToIgnore).until(waitCondition);
	}
	
    public WebElement findElementXpath(WebDriver driver, String xpath, long timeout) {
		return waitFor(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), IGNORED_EXCEPTIONS, timeout);
    }
    
    public void click(WebElement element, WebDriver driver) {
        // focus element before click when running in browser.
        focusElement(element, driver);
        element.click();
    }

    public void focusElement(WebElement element, WebDriver driver) {
        // focus element using javascript focus()
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].focus();", element);
    }

	public void sleep(long msec)
	{
		try {
			Thread.sleep(msec);
		} catch (InterruptedException e) {
		}
	}
	
	public List<WebElement> getFramePath(WebDriver driver, String frameName, String frameType) {
		// switch to the top level frame to traverse the frames tree
		driver.switchTo().defaultContent();
		List<WebElement> framePath = findFrames(driver, frameName, frameType);
		Collections.reverse(framePath);
		return framePath;
	}

	private static List<WebElement> findFrames(SearchContext context, String frameName, String frameType) {
		List<WebElement> requiredFramePath = new LinkedList<>();
		List<WebElement> frames = context.findElements(By.tagName(frameType));
		for (WebElement frame : frames) {
			
			if (frameName.equals(frame.getAttribute("id")) || frameName.equals(frame.getAttribute("name")) || frameName.replace("&amp;", "&").equals(frame.getAttribute("src"))) {
				requiredFramePath.add(frame);
			} else {
				requiredFramePath.addAll(findFrames(frame, frameName, frameType));
			}
		}
		return requiredFramePath;
	}

	public WebDriver switchToFrameByPath(WebDriver driver, List<WebElement> pathToFrame, long timeout) {
		driver = driver.switchTo().defaultContent();
		for (WebElement frame : pathToFrame) {
			driver = switchFrame(driver, frame, timeout);
		}
		
		return driver;
	}

	// wait for specific frame by name or id
	// this works only for one layer below
	public WebDriver switchFrame(WebDriver driver, WebElement frame, long timeout) {
		return waitFor(driver, ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame), IGNORED_EXCEPTIONS, timeout);
	}

}
