package ch304;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IgnoreTest {
	 
    @Test(enabled = false)
    public void testMethod1() {
    	System.out.println("testMethod1()");
    	Assert.assertTrue(true);
    }
 
    @Test
    public void testMethod2() {
    	System.out.println("testMethod2()");
    	Assert.assertTrue(true);
    }
}