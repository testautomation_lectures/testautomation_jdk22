package browserOptions;

import org.openqa.selenium.WebDriver;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class BrowserFunctionsTest {
	  @Test
	  public void setMaximizedWindows() {
	    ChromeOptions chromeOptions = new ChromeOptions();
	    chromeOptions.addArguments("start-maximized");
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
	      // Navigate to Url with maximized windows size
	      driver.get("https://selenium.dev");
	    } finally {
	      driver.quit();
	    }
	  }

	  @Test
	  public void setSpecificSizedWindows() {
	    ChromeOptions chromeOptions = new ChromeOptions();
	    chromeOptions.addArguments("window-size=1000,800");
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
		  // Navigate to Url with sized windows
	      driver.get("https://selenium.dev");
	    } finally {
	      driver.quit();
	    }
	  }
	  
	  @Test
	  public void disableDialogWindows() {
	    ChromeOptions chromeOptions = new ChromeOptions();
		// Disable any popup dialog
	    chromeOptions.setExperimentalOption("excludeSwitches", Arrays.asList("disable-popup-blocking"));
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
		  // Navigate to Url with sized windows
	      driver.get("https://selenium.dev");
	    } finally {
	      driver.quit();
	    }
	  }
	  
	  
	  @Test
	  public void setDownloadPath() {
	    ChromeOptions chromeOptions = new ChromeOptions();
	    Map<String, Object> prefs = new HashMap<String, Object>();
	    prefs.put("download.default_directory", "/directory/path");
	    chromeOptions.setExperimentalOption("prefs", prefs);
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
		  // Navigate to Url with sized windows
	      driver.get("https://selenium.dev");
	    } finally {
	      driver.quit();
	    }
	  }

	  @Test
	  public void RemoveAutomationMessage() {
	    ChromeOptions chromeOptions = new ChromeOptions();
		// Set disable the message "Chrome is being controlled by automated test software"
	    chromeOptions.setExperimentalOption("useAutomationExtension", false);
	    chromeOptions.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));    
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
	      driver.get("https://selenium.dev");
	      Thread.sleep(3000);
	    } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	      driver.quit();
	    }
	  }

	  @Test
	  public void addExtnesion() {
	    ChromeOptions chromeOptions = new ChromeOptions();
	    Path path = Paths.get("src/test/resources/extensions/webextensions-selenium-example.crx");
	    File extensionFilePath = new File(path.toUri());
	    // add Extensions with this browser.
	    chromeOptions.addExtensions(extensionFilePath);
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
	      // Navigate to Url with Extensions
		  driver.get("https://www.selenium.dev/selenium/web/blank.html");
	    } finally {
	      driver.quit();
	    }
	  }
}
