# TestAutomation_JDK22

## Objective

테스트 자동화 시작반 - 소스 공유장소(JDK22용).

## Folder schema

|   Folders            |         Comments                        |
|---------------------|-----------------------------------------|
| /src/test/java       | Test scripts                            |
| /src/test/resources | Test configuration of execution         |


## Change log (reverse chronological order)
***

### 1.0.0 (Jun 24, 2024)
> Upgrade to JDK22 version

### 0.1.0 (May 13, 2024)
> Import JDK17 version and Upgrade Maven/JDK/plugins